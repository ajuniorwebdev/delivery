<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('status')
            ->insert([
                [ 'name' => 'Aberto', 'created_at' => now(), 'updated_at' => now() ],
                [ 'name' => 'Fechado', 'created_at' => now(), 'updated_at' => now() ]
            ]);

        DB::table('store')
            ->insert([
                'name' => 'Pizzaria Bauru', 
                'status_id' => 1, 
                'phone' => '(99) 99999-9999', 
                'address' => 'Rua do endereço da pizzaria', 
                'number' => 9999,
                'district' => 'Bairro de Bauru',
                'created_at' => now(), 
                'updated_at' => now()
            ]);
    }
}

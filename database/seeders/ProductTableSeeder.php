<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')
            ->insert([
                [ 'name' => 'Pizzas', 'created_at' => now(), 'updated_at' => now() ]
            ]);

        DB::table('product')
            ->insert([
                [
                    'category_id' => 1, 
                    'name' => 'Calabresa', 
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor cursus cursus. Nam lacinia porta accumsan. Fusce sagittis orci arcu, a eleifend augue consequat at.', 
                    'image' => 'pizza_calabresa.jpg', 
                    'time_from' => 20, 
                    'time_to' => 30, 
                    'price' => 49.90, 
                    'created_at' => now(), 
                    'updated_at' => now()
                ],
                [
                    'category_id' => 1, 
                    'name' => 'Portuguesa', 
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor cursus cursus. Nam lacinia porta accumsan. Fusce sagittis orci arcu, a eleifend augue consequat at.', 
                    'image' => 'pizza_portuguesa.jpg', 
                    'time_from' => 20, 
                    'time_to' => 30, 
                    'price' => 39.90, 
                    'created_at' => now(), 
                    'updated_at' => now()
                ],
                [
                    'category_id' => 1, 
                    'name' => 'Mussarela', 
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor cursus cursus. Nam lacinia porta accumsan. Fusce sagittis orci arcu, a eleifend augue consequat at.', 
                    'image' => 'pizza_mussarela.jpg', 
                    'time_from' => 20, 
                    'time_to' => 30, 
                    'price' => 39.90, 
                    'created_at' => now(), 
                    'updated_at' => now()
                ],
                [
                    'category_id' => 1, 
                    'name' => 'Frango com Catupiry', 
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor cursus cursus. Nam lacinia porta accumsan. Fusce sagittis orci arcu, a eleifend augue consequat at.', 
                    'image' => 'pizza_frango_catupiry.jpg', 
                    'time_from' => 20, 
                    'time_to' => 30, 
                    'price' => 59.90, 
                    'created_at' => now(), 
                    'updated_at' => now()
                ],
                [
                    'category_id' => 1, 
                    'name' => 'Marguerita', 
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor cursus cursus. Nam lacinia porta accumsan. Fusce sagittis orci arcu, a eleifend augue consequat at.', 
                    'image' => 'pizza_marguerita.jpg', 
                    'time_from' => 20, 
                    'time_to' => 30, 
                    'price' => 59.90, 
                    'created_at' => now(), 
                    'updated_at' => now()
                ],
                [
                    'category_id' => 1, 
                    'name' => 'Napolitana', 
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor cursus cursus. Nam lacinia porta accumsan. Fusce sagittis orci arcu, a eleifend augue consequat at.', 
                    'image' => 'pizza_napolitana.jpg', 
                    'time_from' => 20, 
                    'time_to' => 30, 
                    'price' => 79.90, 
                    'created_at' => now(), 
                    'updated_at' => now()
                ],
                [
                    'category_id' => 1, 
                    'name' => 'Califórnia', 
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor cursus cursus. Nam lacinia porta accumsan. Fusce sagittis orci arcu, a eleifend augue consequat at.', 
                    'image' => 'pizza_california.jpg', 
                    'time_from' => 20, 
                    'time_to' => 30, 
                    'price' => 69.90, 
                    'created_at' => now(), 
                    'updated_at' => now()
                ],
                [
                    'category_id' => 1, 
                    'name' => 'Brigadeiro', 
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor cursus cursus. Nam lacinia porta accumsan. Fusce sagittis orci arcu, a eleifend augue consequat at.', 
                    'image' => 'pizza_brigadeiro.jpg', 
                    'time_from' => 20, 
                    'time_to' => 30, 
                    'price' => 29.90, 
                    'created_at' => now(), 
                    'updated_at' => now()
                ],
                [
                    'category_id' => 1, 
                    'name' => 'Romeu e Julieta', 
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor cursus cursus. Nam lacinia porta accumsan. Fusce sagittis orci arcu, a eleifend augue consequat at.', 
                    'image' => 'pizza_romeu_julieta.jpg', 
                    'time_from' => 20, 
                    'time_to' => 30, 
                    'price' => 29.90, 
                    'created_at' => now(), 
                    'updated_at' => now()
                ]
            ]);
    }
}

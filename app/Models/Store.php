<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Status;

class Store extends Model
{
    use HasFactory;

    protected $table = 'store';
    protected $fillable = [
        'name', 'status_id', 'phone', 'address', 'number', 'district', 'created_at', 'updated_at'
    ];

    public function status() {
        return $this->belongsTo('App\Models\Status', 'status_id', 'id');
    }

}

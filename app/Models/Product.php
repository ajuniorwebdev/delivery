<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Category;

class Product extends Model
{
    use HasFactory;

    protected $table = 'product';
    protected $fillable = [
        'category_id', 'name', 'description', 'image', 'time_from', 'time_to', 'price', 'created_at', 'updated_at'
    ];

    public function category() {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }
}

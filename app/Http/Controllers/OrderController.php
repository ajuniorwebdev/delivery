<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Inertia\Inertia;

class OrderController extends Controller
{
    
    public function index() {

        if(Order::where(['store_id' => 1, 'status' => 0])->exists()) {
            $order = Order::where(['store_id' => 1, 'status' => 0])->first();
            $order_items = OrderItem::with('product')->where(['order_id' => $order->id])->get();
        } else {
            $data = [
                'store_id' => 1,
                'qtd' => 0,
                'total' => 0,
                'status' => 0,
                'created_at' => now(),
                'updated_at' => now()
            ];

            if(Order::create($data)) {
                $order = Order::where(['store_id' => 1, 'status' => 0])->first();
                $order_items = OrderItem::with('product')->where(['order_id' => $order->id])->get();
            }
        }

        return Inertia::render('Order', [
            'store' => Store::with('status')->get(),
            'products' => Product::with('category')->get(),
            'order' => $order,
            'order_items' => $order_items
        ]);
    }

    public function store(Request $request) {
        
        // retorrna qual é o produto e seus valores
        $product = Product::where('id', $request->input('id'))->first();

        // verifica se já existe um pedido aberto para o cliente
        if(Order::where(['store_id' => $request->input('store'), 'status' => 0])->exists()) {
            $order_id = Order::where(['store_id' => $request->input('store'), 'status' => 0])->first()->id;

            // verifica se existe já existe no pedido
            if(OrderItem::where(['order_id' => $order_id, 'product_id' => $product->id])->exists()) {
                $order_item = OrderItem::where(['order_id' => $order_id, 'product_id' => $product->id])->first();
    
                $data = [
                    'qtd' => $order_item->qtd + $request->input('qtd'),
                    'total' => $order_item->total + ($product->price * $request->input('qtd')),
                    'updated_at' => now()
                ];
    
                // Atualiza o produto com a soma dos novos valores
                if(OrderItem::where(['order_id' => $order_id, 'product_id' => $product->id])->update($data)) {
                    $this->updateOrder($order_id);
                } 

            } else {
                // criar um novo item com o produto requisitado
                $data = [
                    'order_id' => $order_id,
                    'product_id' => $product->id,
                    'qtd' => $request->input('qtd'),
                    'price' => $product->price,
                    'total' => $product->price * $request->input('qtd'),
                    'created_at' => now(),
                    'updated_at' => now()
                ];
                if(OrderItem::create($data)) {
                    $this->updateOrder($order_id);
                }
            }
        } else {
            $data = [
                'store_id' => $request->input('store'),
                'qtd' => 0,
                'total' => 0,
                'status' => 0,
                'created_at' => now(),
                'updated_at' => now()
            ];

            if(Order::create($data)) {
                $order_id = Order::where(['store_id' => $request->input('store'), 'status' => 0])->first()->id;
            }

            $data = [
                'order_id' => $order_id,
                'product_id' => $product->id,
                'qtd' => $request->input('qtd'),
                'price' => $product->price,
                'total' => $product->price * $request->input('qtd'),
                'created_at' => now(),
                'updated_at' => now()
            ];
            if(OrderItem::create($data)) {
                $this->updateOrder($order_id);
            }

        }

        return $request->wantsJson()
                    ? response()->json(['msg' => 'success'], 200)
                    : back(); 

    }

    public function destroy(Request $request) {
        if(OrderItem::destroy($request->input('id'))) {
            $this->updateOrder($request->input('order_id'));
            return $request->wantsJson()
                    ? response()->json(['msg' => 'success'], 200)
                    : back();
        }
    }

    public function send(Request $request) {
        if(Order::where(['id' => $request->input('order_id')])->update(['status' => 1, 'updated_at' => now()])) {
            return $request->wantsJson()
                    ? response()->json(['msg' => 'success'], 200)
                    : back();
        }
    }

    protected function updateOrder($order_id) {
        $order_item = OrderItem::where(['order_id' => $order_id])->get();
                    
        $qtd = 0;
        $total = 0;
        foreach($order_item as $item) {
            $qtd += $item->qtd;
            $total += $item->total;
        }

        $data = [
            'qtd' => $qtd,
            'total' => $total,
            'updated_at' => now()
        ];
        
        // Atualiza o pedido geral
        return Order::where(['id' => $order_id])->update($data);
    }

}

## Sobre o sistema

Sistema simples com passo de pedidos para delivery de pizzaria.

Antes de rodar o projeto seguir os passos abaixo

- [Configurar .env
- [Criar base de da com o nome "delivery" e atualizar no .env
- Rodar o comando composer install e após npm install && npm run prod

(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Order_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: ['store', 'products', 'order', 'order_items'],
  data: function data() {
    return {
      status: this.store[0].status.id == 1 ? true : false,
      phone: this.store[0].phone,
      address: this.store[0].address,
      number: this.store[0].number,
      district: this.store[0].district,
      text: 'Pedido Finalizado com sucesso!',
      snackbar: false
    };
  },
  methods: {
    remove: function remove(e, id, order_id) {
      this.$inertia.post('/order/destroy', {
        id: id,
        order_id: order_id
      }, {
        preserveScroll: true
      });
    },
    add: function add(e, id) {
      this.$inertia.post('/order/store', {
        id: id,
        qtd: 1,
        store: 1
      }, {
        preserveScroll: true
      });
    },
    send: function send(e, order_id) {
      var _this = this;

      if (this.order_items.length) {
        this.$inertia.post('/order/send', {
          order_id: order_id
        }, {
          onSuccess: function onSuccess() {
            _this.snackbar = true;
          }
        });
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.img-logo[data-v-4f20b075] {\r\n    height: 200px;\r\n    width: 200px;\r\n    border-radius: 50% !important;\r\n    display: flex !important;\r\n    align-items: center !important;\r\n    align-content: center !important;\r\n    margin: 50px auto 0 auto;\n}\n.card-footer[data-v-4f20b075] {\r\n    width: 100%;\r\n    height: 50px;\r\n    background-color: white;\r\n    position: absolute;\r\n    display: flex;\r\n    align-self: unset;\r\n    align-items: center;\r\n    align-content: center;\r\n    bottom: 0;\r\n    border: 1px solid white;\r\n    padding: 0 20px;\n}\n.order[data-v-4f20b075] {\r\n    bottom: 90px;\r\n    display: block;\n}\n.card-footer .summary[data-v-4f20b075] {\r\n    margin-bottom: 10px;\n}\n.card-footer .summary span[data-v-4f20b075] {\r\n    display: block;\r\n    margin-bottom: 5px;\r\n    font-size: 1.25rem;\r\n    line-height: 1.75rem;\n}\n.card-footer .send-order button[data-v-4f20b075] {\r\n    display: block;\n}\n.custom-btn[data-v-4f20b075] {\r\n    position: absolute;\r\n    right: 15px;\n}\n.custom-plus-icon[data-v-4f20b075] {\r\n    display: flex;\r\n    align-items: center;\r\n    align-content: center;\r\n    height: 100%;\n}\n.custom-plus-icon i[data-v-4f20b075] {\r\n    display: block;\r\n    margin: 0 auto;\n}\r\n\r\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_4f20b075_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_4f20b075_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__.default, options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_4f20b075_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__.default.locals || {});

/***/ }),

/***/ "./resources/js/Pages/Order.vue":
/*!**************************************!*\
  !*** ./resources/js/Pages/Order.vue ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Order_vue_vue_type_template_id_4f20b075_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Order.vue?vue&type=template&id=4f20b075&scoped=true& */ "./resources/js/Pages/Order.vue?vue&type=template&id=4f20b075&scoped=true&");
/* harmony import */ var _Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Order.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Order.vue?vue&type=script&lang=js&");
/* harmony import */ var _Order_vue_vue_type_style_index_0_id_4f20b075_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css& */ "./resources/js/Pages/Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Order_vue_vue_type_template_id_4f20b075_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _Order_vue_vue_type_template_id_4f20b075_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "4f20b075",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Order.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Order.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./resources/js/Pages/Order.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Order.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/Pages/Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css& ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_4f20b075_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader/dist/cjs.js!../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=style&index=0&id=4f20b075&scoped=true&lang=css&");


/***/ }),

/***/ "./resources/js/Pages/Order.vue?vue&type=template&id=4f20b075&scoped=true&":
/*!*********************************************************************************!*\
  !*** ./resources/js/Pages/Order.vue?vue&type=template&id=4f20b075&scoped=true& ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_4f20b075_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_4f20b075_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_4f20b075_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Order.vue?vue&type=template&id=4f20b075&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=template&id=4f20b075&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=template&id=4f20b075&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Order.vue?vue&type=template&id=4f20b075&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "app" } },
    [
      _c(
        "v-app",
        [
          _c(
            "v-app-bar",
            { attrs: { app: "" } },
            [
              _c("v-toolbar-title", [_vm._v("Cardápio")]),
              _vm._v(" "),
              _c("v-spacer"),
              _vm._v(" "),
              _c(
                "v-btn",
                { attrs: { icon: "" } },
                [_c("v-icon", [_vm._v("mdi-login")])],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-main",
            [
              _c(
                "v-container",
                { attrs: { fluid: "" } },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { md: "9" } },
                        [
                          _c(
                            "v-row",
                            [
                              _c(
                                "v-col",
                                { attrs: { md: "12" } },
                                [
                                  _c(
                                    "v-card",
                                    {
                                      attrs: {
                                        height: "400px",
                                        color: "grey light-400"
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "img-logo bg-white"
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "card-footer" },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "mr-5",
                                              class: _vm.status
                                                ? "text-green-800"
                                                : "text-red-800"
                                            },
                                            [
                                              _vm.status
                                                ? [
                                                    _c(
                                                      "p",
                                                      { staticClass: "mb-0" },
                                                      [
                                                        _c(
                                                          "v-icon",
                                                          {
                                                            attrs: {
                                                              size: "20",
                                                              color:
                                                                "green light-600"
                                                            }
                                                          },
                                                          [
                                                            _vm._v(
                                                              "mdi-thumb-up-outline"
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" Aberto")
                                                      ],
                                                      1
                                                    )
                                                  ]
                                                : [
                                                    _c(
                                                      "p",
                                                      { staticClass: "mb-0" },
                                                      [
                                                        _c(
                                                          "v-icon",
                                                          {
                                                            attrs: {
                                                              size: "20",
                                                              color:
                                                                "red light-600"
                                                            }
                                                          },
                                                          [
                                                            _vm._v(
                                                              "mdi-thumb-down-outline"
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" Fechado")
                                                      ],
                                                      1
                                                    )
                                                  ]
                                            ],
                                            2
                                          ),
                                          _vm._v(" "),
                                          _c("div", { staticClass: "mr-5" }, [
                                            _c(
                                              "p",
                                              { staticClass: "mb-0" },
                                              [
                                                _c(
                                                  "v-icon",
                                                  { attrs: { size: "20" } },
                                                  [
                                                    _vm._v(
                                                      "mdi-phone-message-outline"
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" " + _vm._s(_vm.phone))
                                              ],
                                              1
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("div", { staticClass: "mr-5" }, [
                                            _c(
                                              "p",
                                              { staticClass: "mb-0" },
                                              [
                                                _c(
                                                  "v-icon",
                                                  { attrs: { size: "20" } },
                                                  [
                                                    _vm._v(
                                                      "mdi-map-marker-outline"
                                                    )
                                                  ]
                                                ),
                                                _vm._v(
                                                  " " +
                                                    _vm._s(_vm.address) +
                                                    ", " +
                                                    _vm._s(_vm.number) +
                                                    ", " +
                                                    _vm._s(_vm.district)
                                                )
                                              ],
                                              1
                                            )
                                          ])
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-row",
                            [
                              _c(
                                "v-col",
                                { attrs: { md: "12" } },
                                [
                                  _c(
                                    "v-card",
                                    [
                                      _c("v-card-title", [_vm._v("Pizzas")]),
                                      _vm._v(" "),
                                      _c("v-divider"),
                                      _vm._v(" "),
                                      _c(
                                        "v-card-text",
                                        [
                                          _c(
                                            "v-row",
                                            _vm._l(_vm.products, function(
                                              product
                                            ) {
                                              return _c(
                                                "v-col",
                                                {
                                                  key: product.id,
                                                  attrs: { md: "4" }
                                                },
                                                [
                                                  _c(
                                                    "v-card",
                                                    [
                                                      _c(
                                                        "v-list-item",
                                                        {
                                                          attrs: {
                                                            "four-line": ""
                                                          }
                                                        },
                                                        [
                                                          _c(
                                                            "v-list-item-avatar",
                                                            {
                                                              attrs: {
                                                                tile: "",
                                                                size: "100",
                                                                color: "grey"
                                                              }
                                                            },
                                                            [
                                                              _c("v-img", {
                                                                attrs: {
                                                                  src:
                                                                    "/image/" +
                                                                    product.image
                                                                }
                                                              })
                                                            ],
                                                            1
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "v-list-item-content",
                                                            [
                                                              _c(
                                                                "v-list-item-title",
                                                                {
                                                                  staticClass:
                                                                    "headline mb-3"
                                                                },
                                                                [
                                                                  _c(
                                                                    "span",
                                                                    {
                                                                      staticClass:
                                                                        "text-sm"
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "strong",
                                                                        [
                                                                          _vm._v(
                                                                            _vm._s(
                                                                              product.name
                                                                            )
                                                                          )
                                                                        ]
                                                                      )
                                                                    ]
                                                                  ),
                                                                  _vm._v(" "),
                                                                  _c(
                                                                    "v-btn",
                                                                    {
                                                                      staticClass:
                                                                        "custom-btn green light-800",
                                                                      attrs: {
                                                                        dark:
                                                                          "",
                                                                        small:
                                                                          ""
                                                                      },
                                                                      on: {
                                                                        click: function(
                                                                          $event
                                                                        ) {
                                                                          return _vm.add(
                                                                            $event,
                                                                            product.id
                                                                          )
                                                                        }
                                                                      }
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "v-icon",
                                                                        [
                                                                          _vm._v(
                                                                            "mdi-plus"
                                                                          )
                                                                        ]
                                                                      ),
                                                                      _vm._v(
                                                                        " Adicionar"
                                                                      )
                                                                    ],
                                                                    1
                                                                  )
                                                                ],
                                                                1
                                                              ),
                                                              _vm._v(" "),
                                                              _c("div", [
                                                                _c("strong", [
                                                                  _vm._v(
                                                                    "Ingredientes:"
                                                                  )
                                                                ]),
                                                                _vm._v(
                                                                  _vm._s(
                                                                    product.description
                                                                  )
                                                                )
                                                              ]),
                                                              _vm._v(" "),
                                                              _c("div", [
                                                                _c("span", [
                                                                  _c("strong", [
                                                                    _vm._v(
                                                                      "Tempo:"
                                                                    )
                                                                  ]),
                                                                  _vm._v(
                                                                    " " +
                                                                      _vm._s(
                                                                        product.time_from
                                                                      ) +
                                                                      "~" +
                                                                      _vm._s(
                                                                        product.time_to
                                                                      ) +
                                                                      " min"
                                                                  )
                                                                ]),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "span",
                                                                  {
                                                                    staticClass:
                                                                      "float-right text-md"
                                                                  },
                                                                  [
                                                                    _c(
                                                                      "strong",
                                                                      [
                                                                        _vm._v(
                                                                          "R$ " +
                                                                            _vm._s(
                                                                              product.price
                                                                            )
                                                                        )
                                                                      ]
                                                                    )
                                                                  ]
                                                                )
                                                              ])
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            }),
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { staticClass: "h-screen", attrs: { md: "3" } },
                        [
                          _c(
                            "v-card",
                            {
                              staticStyle: { position: "fixed" },
                              attrs: { height: "90%", width: "24%" }
                            },
                            [
                              _c("v-card-title", [_vm._v("Meu Pedido")]),
                              _vm._v(" "),
                              _c("v-divider"),
                              _vm._v(" "),
                              _vm.order_items.length
                                ? _c(
                                    "v-card-text",
                                    _vm._l(_vm.order_items, function(item) {
                                      return _c(
                                        "v-card",
                                        { key: item.id, staticClass: "mb-3" },
                                        [
                                          _c(
                                            "v-list-item",
                                            { attrs: { "four-line": "" } },
                                            [
                                              _c(
                                                "v-list-item-avatar",
                                                {
                                                  attrs: {
                                                    tile: "",
                                                    size: "100",
                                                    color: "grey"
                                                  }
                                                },
                                                [
                                                  _c("v-img", {
                                                    attrs: {
                                                      src:
                                                        "/image/" +
                                                        item.product.image
                                                    }
                                                  })
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "v-list-item-content",
                                                [
                                                  _c(
                                                    "v-list-item-title",
                                                    {
                                                      staticClass:
                                                        "headline mb-3"
                                                    },
                                                    [
                                                      _c(
                                                        "span",
                                                        {
                                                          staticClass: "text-sm"
                                                        },
                                                        [
                                                          _c("strong", [
                                                            _vm._v(
                                                              _vm._s(
                                                                item.product
                                                                  .name
                                                              )
                                                            )
                                                          ])
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "v-btn",
                                                        {
                                                          staticClass:
                                                            "custom-btn red light-800",
                                                          attrs: {
                                                            dark: "",
                                                            small: ""
                                                          },
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return _vm.remove(
                                                                $event,
                                                                item.id,
                                                                item.order_id
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _c("v-icon", [
                                                            _vm._v(
                                                              "mdi-delete-outline"
                                                            )
                                                          ]),
                                                          _vm._v(" Remover")
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c("div", [
                                                    _c("strong", [
                                                      _vm._v("Ingredientes:")
                                                    ]),
                                                    _vm._v(
                                                      " " +
                                                        _vm._s(
                                                          item.product
                                                            .description
                                                        )
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("div", [
                                                    _c("span", [
                                                      _c("strong", [
                                                        _vm._v("Tempo:")
                                                      ]),
                                                      _vm._v(
                                                        " " +
                                                          _vm._s(
                                                            item.product
                                                              .time_from
                                                          ) +
                                                          "~" +
                                                          _vm._s(
                                                            item.product.time_to
                                                          ) +
                                                          " min"
                                                      )
                                                    ]),
                                                    _vm._v(" "),
                                                    _c(
                                                      "span",
                                                      {
                                                        staticClass:
                                                          "float-right text-md"
                                                      },
                                                      [
                                                        _c("strong", [
                                                          _vm._v(
                                                            "R$ " +
                                                              _vm._s(item.total)
                                                          )
                                                        ])
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "span",
                                                      {
                                                        staticClass:
                                                          "float-right text-md mr-2"
                                                      },
                                                      [
                                                        _c("strong", [
                                                          _vm._v(
                                                            "Qtd: " +
                                                              _vm._s(item.qtd)
                                                          )
                                                        ])
                                                      ]
                                                    )
                                                  ])
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    }),
                                    1
                                  )
                                : _c(
                                    "v-card-text",
                                    [
                                      _c(
                                        "v-card",
                                        { staticClass: "mb-3 w-100" },
                                        [
                                          _c("v-card-text", [
                                            _vm._v("Nenhum item no carrinho!")
                                          ])
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _c(
                                "v-card-text",
                                { staticClass: "card-footer order" },
                                [
                                  _c("v-divider", { staticClass: "mb-3" }),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "summary" }, [
                                    _c("span", { staticClass: "total" }, [
                                      _c("strong", [_vm._v("Qtd:")]),
                                      _c(
                                        "span",
                                        { staticClass: "float-right" },
                                        [_vm._v(_vm._s(_vm.order.qtd))]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("span", { staticClass: "total" }, [
                                      _c("strong", [_vm._v("Total:")]),
                                      _c(
                                        "span",
                                        { staticClass: "float-right" },
                                        [
                                          _vm._v(
                                            "R$ " + _vm._s(_vm.order.total)
                                          )
                                        ]
                                      )
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "send-order" },
                                    [
                                      _c(
                                        "v-btn",
                                        {
                                          attrs: {
                                            color: "green light-500 mx-auto",
                                            dark: "",
                                            large: ""
                                          },
                                          on: {
                                            click: function($event) {
                                              return _vm.send(
                                                $event,
                                                _vm.order.id
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c("v-icon", [_vm._v("mdi-check")]),
                                          _vm._v(" Finalizar Pedido")
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-snackbar",
                                        {
                                          scopedSlots: _vm._u([
                                            {
                                              key: "action",
                                              fn: function(ref) {
                                                var attrs = ref.attrs
                                                return [
                                                  _c(
                                                    "v-btn",
                                                    _vm._b(
                                                      {
                                                        attrs: {
                                                          color: "pink",
                                                          text: ""
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            _vm.snackbar = false
                                                          }
                                                        }
                                                      },
                                                      "v-btn",
                                                      attrs,
                                                      false
                                                    ),
                                                    [_vm._v("Close")]
                                                  )
                                                ]
                                              }
                                            }
                                          ]),
                                          model: {
                                            value: _vm.snackbar,
                                            callback: function($$v) {
                                              _vm.snackbar = $$v
                                            },
                                            expression: "snackbar"
                                          }
                                        },
                                        [
                                          _vm._v(
                                            "\n                                        " +
                                              _vm._s(_vm.text) +
                                              "\n                                        "
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ normalizeComponent)
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })

}]);
<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [OrderController::class, 'index']);
Route::post('/order/store', [OrderController::class, 'store']);
Route::post('/order/destroy', [OrderController::class, 'destroy']);
Route::post('/order/send', [OrderController::class, 'send']);
